.PHONY: test

test:
	pytest \
	--verbosity=2 \
	--showlocals \
	--strict-markers \
	--cov=pipeline \
	--cov-config=./pyproject.toml \
	--cov-report term-missing \
	$(arg) \
	-k "$(k)"
