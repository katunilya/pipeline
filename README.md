# ML Devs Pipeline

> Documentation is under development

`mldevs-pipeline` is a Python package that provides a set of basic functions for
writing more functional and readable code, implementing

- Pipe operator via `Pipeline` and `AsyncPipeline` containers
- Maybe monad binding functions `if_some`, `if_none` and `if_none_return`
- Result monad binding functions `if_ok`, `if_error` and `if_error_return`

## Use Cases and Motivation

Fundamental idea behind this package is concept of **Code-As-A-Story** approach.
CaaS is inspired by ideas of Clean Architecture, Domain-Driven Design & CQRS (in
terms of code). There are a few simple ideas behind CaaS which form it's
philosophy:

- *API is more important than implementation*

    By API there we mean function/method signatures in first place.
    Self-explanatory, clean function signature saves a lot of time during
    development, debugging and maintenance of the application.

    In first place this requires coming up with good names for functions and
    their arguments. Also `mldevs-pipeline` pushes developer to write functions
    with single argument, that represents some object that we are processing
    during pipeline execution.

    This instead of writing typical imperative code, we write a *story* for what
    happens during execution of some Use Case.

    Example:

    ```python
    async def get_payment_bill(user_id: UserID, cart_id: CartID) -> PaymentBill:
        cart = await get_cart(cart_id)

        available_discounts = await get_active_discount_programmes(user_id)
        user_discounts = [
            discount for discount in available_discounts if discount.matches(cart)
        ]

        cart = available_discounts.apply(cart)
        price = cart.calculate_price()

        bill = PaymentBill(user, cart, available_discounts)

        await insert_payment_bill(bill)

        return bill
    ```

    This code generally is not bad and enough self-explanatory, as its scenario
    is pretty simple.

    With `mldevs-pipeline` we rewrite this code in following manner:

    ```python
    async def get_payment_bill(message: GetPaymentBill) -> PaymentBill:
        return await (
            Pipeline(message)
            .then_async(get_message_cart)
            .then_async(get_active_discount_programmes)
            .then(filter_user_discounts)
            .then(apply_discounts_to_cart)
            .then(build_payment_bill)
            .then(insert_payment_bill)
            .then(map_to_payment_bill)
            .unpack()
        )
    ```

    With this code we force developers to write business scenarios / use cases
    as such stories that are much more natural and readable, so that other
    developer and even the one who wrote the code initially could understand
    what is happening in the code easier in the future.

- *Domain is separated*

    This idea is dictated by Clean Architecture philosophy. Domain-specific code
    must form foundation of the system and should not be mixed with
    infrastructure and other non-related to domain aspects.

    `Pipeline`-based execution of use cases forces developer to separate this
    aspects from actual domain-specific code via wrapping pipelines for example.

    Thus we force using concept of Aspect-oriented development, which separates
    all infrastructure logic from domain use cases.

Some implementation details are questionable especially when we talk about
provided implementations for "monads". We assume that the main question will be
\- why no traditional containers provided for Maybe and Result monads?

General idea is not to have multiple options for solving the same problem.
Python already provides Exceptions and None value, so there is no point to
provide duplicates. Because of this we encourage using built-in `Exception`
class and `None`, which also requires less resources to migrate from existing
code base to use `mldevs-pipeline`.

## Development

### Requirements

- `python3.12`
- `poetry`
- `make`

### Getting Started

> TODO add commands to execute

1. Clone project from git repository (better with SSH)

    ```sh
    git clone git@gitlab.com:ml.devs/inner-source/mldevs-pipeline.git
    ```

2. Create **Python3.12** virtual environment and activate it for `poetry`

    ```sh
    poetry env use 3.12
    poetry shell
    ```

3. Install project dependencies with `poetry`

    ```sh
    poetry install
    ```

4. Install `pre-commit` hooks

    ```sh
    poetry run pre-commit install --hook-type pre-commit --hook-type pre-push --hook-type commit-msg
    ```

### Development cycle

1. Create issue on GitLab
2. Create branch form `main` & Merge Request with Draft status (can be done in
   one-click on GitLab)
3. Pull branch to local repository & checkout
4. Develop
5. Commit with `commitizen`
6. Ask for review
7. Merge!

### FAQ

1. How to add new dependency?

    To add new dependency use `poetry add` and do not forge group flag
    (`-G/--group`):
    - *no flag* - for production dependencies
    - *dev* - for development dependencies (local tools like linters, formatter,
      etc.)
    - *test* - for dependencies required for testing

2. How to run tests?

    Just use `make test` (possibly `poetry run make test`)

3. How to configure my IDE?

    For VS Code users there are a few recommended actions:
    1. Uninstall extension for `flake8`, `black`, `mypy` and `isort`
    2. Install extension for `ruff`

    Currently `mypy` does not support `python3.12` generic syntax (PEP 695),
    `black` and `isort` would conflict with `ruff`.
