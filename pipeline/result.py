from functools import wraps
from typing import Awaitable, Callable

__all__ = [
    "if_ok",
    "if_error",
    "if_error_return",
    "safe",
    "safe_async",
]


def if_ok[T, R](callable: Callable[[T], R]) -> Callable[[T | Exception], R | Exception]:
    def _if_ok(value: T | Exception) -> R | Exception:
        if isinstance(value, Exception):
            return value

        return callable(value)

    return _if_ok


def if_error[T, R](
    callable: Callable[[Exception], R]
) -> Callable[[T | Exception], R | T]:
    def _if_error(value: Exception | T) -> R | T:
        if isinstance(value, Exception):
            return callable(value)

        return value

    return _if_error


def if_error_return[T, R](replacement: R) -> Callable[[T | Exception], T | R]:
    def _if_error_return(value: T | Exception) -> T | R:
        if isinstance(value, Exception):
            return replacement

        return value

    return _if_error_return


def safe[**P, R](func: Callable[P, R]) -> Callable[P, R | Exception]:
    @wraps(func)
    def _safe(*args: P.args, **kwargs: P.kwargs) -> R | Exception:
        try:
            return func(*args, **kwargs)
        except Exception as exc:
            return exc

    return _safe


def safe_async[**P, R](
    func: Callable[P, Awaitable[R]]
) -> Callable[P, Awaitable[R | Exception]]:
    @wraps(func)
    async def _safe_async(*args: P.args, **kwargs: P.kwargs) -> R | Exception:
        try:
            return await func(*args, **kwargs)
        except Exception as exc:
            return exc

    return _safe_async

    ...
