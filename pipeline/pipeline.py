from dataclasses import dataclass
from inspect import isawaitable
from typing import Awaitable, Callable

__all__ = [
    "AsyncPipeline",
    "Pipeline",
]


async def _common_process[T, R](
    value: Awaitable[T], processor: Callable[[T], R] | Callable[[T], Awaitable[R]]
) -> R:
    if isawaitable(value):
        value = await value

    result = processor(value)  # type: ignore

    return await result if isawaitable(result) else result  # type: ignore


@dataclass(slots=True)
class AsyncPipeline[T]:
    value: Awaitable[T]

    def then[R](
        self,
        processor: Callable[[T], R],
    ) -> "AsyncPipeline[R]":
        return AsyncPipeline(_common_process(self.value, processor))

    def then_async[R](
        self,
        async_processor: Callable[[T], Awaitable[R]],
    ) -> "AsyncPipeline[R]":
        return AsyncPipeline(_common_process(self.value, async_processor))

    def unpack(self) -> Awaitable[T]:
        return self.value


@dataclass(slots=True)
class Pipeline[T]:
    value: T

    def then[R](
        self,
        processor: Callable[[T], R],
    ) -> "Pipeline[R]":
        return Pipeline(processor(self.value))

    def then_async[R](
        self,
        async_processor: Callable[[T], Awaitable[R]],
    ) -> "AsyncPipeline[R]":
        return AsyncPipeline(_common_process(self.value, async_processor))  # type: ignore

    def unpack(self) -> T:
        return self.value
