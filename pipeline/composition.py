from dataclasses import dataclass
from inspect import isawaitable
from typing import Awaitable, Callable

__all__ = [
    "AsyncComposition",
    "Composition",
]


@dataclass(slots=True)
class AsyncComposition[_ValueType, _ResultType]:
    _func: Callable[[_ValueType], Awaitable[_ResultType]]

    async def __call__(self, value: _ValueType) -> _ResultType:
        return await self._func(value)

    def then[_NewResultType](self, func: Callable[[_ResultType], _NewResultType]):
        async def _then(value: _ValueType) -> _NewResultType:
            if isawaitable(value):
                value = await value  # pragma: no cover

            result = self._func(value)  # type: ignore

            if isawaitable(result):
                result = await result

            new_result = func(result)  # type: ignore

            if isawaitable(new_result):
                return await new_result  # pragma: no cover

            return new_result

        return AsyncComposition[_ValueType, _NewResultType](_then)

    def then_async[_NewResultType](
        self, func: Callable[[_ResultType], Awaitable[_NewResultType]]
    ):
        async def _then_async(value: _ValueType) -> _NewResultType:
            if isawaitable(value):
                value = await value

            result = self._func(value)

            if isawaitable(result):
                result = await result

            new_result = func(result)  # type: ignore

            if isawaitable(new_result):
                return await new_result

            return new_result  # type: ignore

        return AsyncComposition[_ValueType, _NewResultType](_then_async)


@dataclass(slots=True)
class Composition[_ValueType, _ResultType]:
    _func: Callable[[_ValueType], _ResultType]

    def __call__(self, value: _ValueType) -> _ResultType:
        return self._func(value)

    def then[_NewResultType](self, func: Callable[[_ResultType], _NewResultType]):
        return Composition[_ValueType, _NewResultType](lambda v: func(self._func(v)))

    def then_async[_NewResultType](
        self, func: Callable[[_ResultType], Awaitable[_NewResultType]]
    ):
        async def _then_async(value: _ValueType) -> _NewResultType:
            if isawaitable(value):
                value = await value  # pragma: no cover

            result = self._func(value)

            if isawaitable(result):
                result = await result  # pragma: no cover

            new_result = func(result)

            if isawaitable(new_result):
                return await new_result  # pragma: no cover

            return new_result  # type: ignore

        return AsyncComposition[_ValueType, _NewResultType](_then_async)
