from typing import Any, Callable

__all__ = [
    "if_some",
    "if_none",
    "if_none_return",
]


def if_some[T, R](callable: Callable[[T], R]) -> Callable[[T | None], R | None]:
    def _if_some(value: T | None) -> R | None:
        if value:
            return callable(value)

        return None

    return _if_some


def if_none[T, R](callable: Callable[[Any], R]) -> Callable[[T | None], R | T]:
    def _if_none(value: T | None) -> R | T:
        if value:
            return value

        return callable(None)

    return _if_none


def if_none_return[T, R](replacement: R) -> Callable[[T | None], R | T]:
    def _if_none_return(value: T | None) -> R | T:
        if not value:
            return replacement
        return value

    return _if_none_return
