import asyncio
import functools
from concurrent.futures import ThreadPoolExecutor, as_completed
from typing import Awaitable, Callable, Iterable, Mapping

__all__ = [
    "group_by",
    "group_by_async",
    "filter",
    "filter_async",
    "valmap",
    "valmap_async",
    "valmap_threaded",
    "skip",
    "reduce",
]


def group_by[V, K](grouper: Callable[[V], K]):
    def _group_by(data: Iterable[V]) -> Mapping[K, Iterable[V]]:
        result = dict[K, list[V]]()

        for item in data:
            key = grouper(item)
            if key not in result:
                result[key] = []

            result[key].append(item)
        return result

    return _group_by


def group_by_async[V, K](grouper: Callable[[V], Awaitable[K]]):
    async def _group_by_async(data: Iterable[V]) -> Mapping[K, Iterable[V]]:
        result = dict[K, list[V]]()

        for item in data:
            key = await grouper(item)
            if key not in result:
                result[key] = []

            result[key].append(item)
        return result

    return _group_by_async


def filter[V](predicate: Callable[[V], bool]):
    def _filter(data: Iterable[V]):
        for item in data:
            if predicate(item):
                yield item

    return _filter


def filter_async[V](predicate: Callable[[V], Awaitable[bool]]):
    async def _filter_async(data: Iterable[V]) -> Iterable[V]:
        result = list[V]()
        for item in data:
            if await predicate(item):
                result.append(item)

        return result

    return _filter_async


def valmap[V, R](mapper: Callable[[V], R]):
    def _valmap(data: Iterable[V]):
        for item in data:
            yield mapper(item)

    return _valmap


def valmap_async[V, R](mapper: Callable[[V], Awaitable[R]]):
    async def _valmap_async(data: Iterable[V]):
        return await asyncio.gather(*[mapper(item) for item in data])

    return _valmap_async


def valmap_threaded[V, R](
    mapper: Callable[[V], R],
    *,
    max_workers: int | None = None,
    thread_name_prefix: str = "",
):
    def _valmap_threaded(data: Iterable[V]):
        result = list[R]()
        with ThreadPoolExecutor(max_workers, thread_name_prefix) as executor:
            futures = [executor.submit(mapper, item) for item in data]

            for future in as_completed(futures):
                result.append(future.result())

        return result

    return _valmap_threaded


def skip[V](amount: int):
    amount = amount if amount >= 0 else 0

    def _skip(data: Iterable[V]):
        for i, item in enumerate(data):
            if i >= amount:
                yield item

    return _skip


_InitialMissing = object()


def reduce[A, V](
    func: Callable[[A, V], A],
    *,
    initial: A = _InitialMissing,
) -> Callable[[Iterable[V]], A]:
    return (
        lambda iterable: functools.reduce(func, iterable)  # type: ignore
        if initial is _InitialMissing
        else functools.reduce(func, iterable, initial)
    )  # type: ignore
