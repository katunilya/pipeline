from typing import Any, Callable

__all__ = [
    "identity",
    "async_identity",
    "replace",
    "when",
]


def identity[T](value: T) -> T:
    return value


async def async_identity[T](value: T) -> T:
    return value


def replace[R](replacement: R) -> Callable[[Any], R]:
    return lambda _: replacement


def when[V, R](predicate: bool | Callable[[V], bool], func: Callable[[V], R]):
    _predicate = replace(predicate) if isinstance(predicate, bool) else predicate

    def _when(data: V):
        if _predicate(data):
            return func(data)

        return data

    return _when
