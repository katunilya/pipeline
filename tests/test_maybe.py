import pytest
from pipeline.maybe import if_none, if_none_return, if_some
from pipeline.pipeline import Pipeline


@pytest.mark.parametrize(
    ("value", "function", "expected"),
    [
        (1, lambda x: x + 1, 2),
        (1, lambda _: None, None),
        (None, lambda x: x + 1, None),
        (None, lambda _: None, None),
    ],
)
def test_if_some(value, function, expected):
    result = Pipeline((value)).then(if_some(function)).unpack()

    assert (
        result == expected
    ), f"if_some did not return expected result: {expected=} {result=}"


@pytest.mark.parametrize(
    ("value", "function", "expected"),
    [
        (1, lambda x: x + 1, 1),
        (1, lambda _: None, 1),
        (None, lambda _: 1, 1),
        (None, lambda _: None, None),
    ],
)
def test_if_none(value, function, expected):
    result = Pipeline((value)).then(if_none((function))).unpack()

    assert (
        result == expected
    ), f"if_some did not return expected result: {expected=} {result=}"


@pytest.mark.parametrize(
    ("value", "replacement", "expected"),
    [
        (1, 2, 1),
        (None, None, None),
        (None, 2, 2),
    ],
)
def test_if_none_return(value, replacement, expected):
    result = Pipeline(value).then(if_none_return(replacement)).unpack()

    assert (
        result == expected
    ), f"if_none_return did not return expected result: {expected=} {result=}"
