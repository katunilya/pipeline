from inspect import isawaitable

import hypothesis.strategies as st
import pytest
from hypothesis import given

from pipeline.common import async_identity, identity
from pipeline.composition import AsyncComposition, Composition
from pipeline.result import if_ok
from tests.conftest import add, async_add, async_sub, sub


@given(value=st.integers())
def test_composition(value: int):
    expected = value + 1 - 1

    result = Composition(add(1)).then(sub(1))(value)

    assert (
        result == expected
    ), f"Composition does not produce expected result: {expected=} {result=}"


@pytest.mark.asyncio()
@given(value=st.integers())
async def test_async_composition(value: int):
    expected = value + 1 - 1

    result = (
        AsyncComposition(async_add(1))
        .then_async(async_sub(1))
        .then(add(1))
        .then(sub(1))
    )(value)

    assert isawaitable(result), "AsyncComposition returned non-awaitable object"
    result = await result
    assert (
        result == expected
    ), f"AsyncComposition does not produce expected result: {expected=} {result=}"


@pytest.mark.asyncio()
async def test_async_composition_with_non_possibly_non_async_result():
    await (
        Composition[Exception, Exception](identity)
        .then_async(if_ok(async_add(1)))  # type: ignore
        .then_async(if_ok(async_add(1)))  # type: ignore
    )(Exception())


@pytest.mark.asyncio()
async def test_composition_accidental_async_value():
    func = AsyncComposition[int, int](async_identity).then_async(async_add(1))
    result = await func(async_identity(0))  # type: ignore

    assert result == 1
