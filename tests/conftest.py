from typing import Any, Awaitable, Callable

import pytest


def add(a: int) -> Callable[[int], int]:
    return lambda b: a + b


def sub(a: int) -> Callable[[int], int]:
    return lambda b: b - a


def async_add(a: int) -> Callable[[int], Awaitable[int]]:
    async def _async_add(b: int) -> int:
        return a + b

    return _async_add


def async_sub(a: int) -> Callable[[int], Awaitable[int]]:
    async def _async_sub(b: int) -> int:
        return b - a

    return _async_sub


@pytest.fixture()
def test_exception() -> Exception:
    return Exception("test_exception")


@pytest.fixture()
def test_exception_function(test_exception: Exception) -> Callable[[Any], Exception]:
    return lambda _: test_exception


def divide(a: int) -> Callable[[int], int]:
    return lambda b: b // a


def divide_async(a: int) -> Callable[[int], Awaitable[int]]:
    async def _divide_async(b: int):
        return b // a

    return _divide_async
