import pytest

from pipeline.pipeline import Pipeline
from pipeline.result import if_error, if_error_return, if_ok, safe, safe_async
from tests.conftest import divide, divide_async

test_exception = Exception("te")


@pytest.mark.parametrize(
    ("value", "function", "expected"),
    [
        (1, lambda x: x + 1, 2),  # type: ignore
        (
            1,
            pytest.lazy_fixture("test_exception_function"),  # type: ignore
            pytest.lazy_fixture("test_exception"),  # type: ignore
        ),
        (
            pytest.lazy_fixture("test_exception"),  # type: ignore
            lambda: 1,
            pytest.lazy_fixture("test_exception"),  # type: ignore
        ),
        (
            pytest.lazy_fixture("test_exception"),  # type: ignore
            pytest.lazy_fixture("test_exception_function"),  # type: ignore
            pytest.lazy_fixture("test_exception"),  # type: ignore
        ),
    ],
)
def test_if_ok(value, function, expected):  # type: ignore
    result = Pipeline(value).then(if_ok(function)).unpack()  # type: ignore

    assert (
        result == expected
    ), f"if_ok did not return expected result: {expected=} {result=}"


@pytest.mark.parametrize(
    ("value", "function", "expected"),
    [
        (1, lambda x: x + 1, 1),  # type: ignore
        (
            1,
            pytest.lazy_fixture("test_exception_function"),  # type: ignore
            1,
        ),
        (
            pytest.lazy_fixture("test_exception"),  # type: ignore
            lambda _: 1,  # type: ignore
            1,
        ),
        (
            pytest.lazy_fixture("test_exception"),  # type: ignore
            pytest.lazy_fixture("test_exception_function"),  # type: ignore
            pytest.lazy_fixture("test_exception"),  # type: ignore
        ),
    ],
)
def test_if_error(value, function, expected):  # type: ignore
    result = Pipeline(value).then(if_error(function)).unpack()  # type: ignore

    assert (
        result == expected
    ), f"if_error did not return expected result: {expected=} {result=}"


@pytest.mark.parametrize(
    ("value", "replacement", "expected"),
    [
        (1, 2, 1),
        (pytest.lazy_fixture("test_exception"), None, None),  # type: ignore
        (pytest.lazy_fixture("test_exception"), 2, 2),  # type: ignore
    ],
)
def test_if_error_return(
    value: int | Exception, replacement: int | None, expected: int | None | Exception
):
    result = Pipeline(value).then(if_error_return(replacement)).unpack()  # type: ignore

    assert (
        result == expected
    ), f"if_error_return did not return expected result: {expected=} {result=}"


def test_safe():
    divide_by_0 = divide(0)
    with pytest.raises(ZeroDivisionError):
        divide_by_0(10)

    safe_divide_by_0 = safe(divide_by_0)

    assert isinstance(safe_divide_by_0(10), ZeroDivisionError)


@pytest.mark.asyncio
async def test_safe_async():
    divide_by_0 = divide_async(0)
    with pytest.raises(ZeroDivisionError):
        await divide_by_0(10)

    safe_divide_by_0 = safe_async(divide_by_0)

    assert isinstance(await safe_divide_by_0(10), ZeroDivisionError)
