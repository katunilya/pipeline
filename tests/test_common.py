import hypothesis.strategies as st
import pytest
from hypothesis import given

from pipeline.common import async_identity, identity, replace, when
from pipeline.pipeline import Pipeline
from tests.conftest import add


@given(value=st.integers())
def test_identity(value: int):
    assert value == identity(value)


@pytest.mark.asyncio()
@given(value=st.integers())
async def test_async_identity(value: int):
    assert value == await async_identity(value)


@given(replacement=st.integers(), value=st.integers())
def test_replace(replacement: int, value: int):
    replacer = replace(replacement)

    assert replacer(value) == replacement


@pytest.mark.parametrize(
    "predicate, target",
    [
        (replace(True), 1),
        (replace(False), 0),
        (True, 1),
        (False, 0),
    ],
)
def test_when(predicate, target):
    assert Pipeline(0).then(when(predicate, add(1))).unpack() == target
