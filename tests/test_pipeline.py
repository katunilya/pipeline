from inspect import isawaitable

import hypothesis.strategies as st
import pytest
from hypothesis import given

from pipeline.pipeline import Pipeline
from pipeline.result import if_ok
from tests.conftest import add, async_add, async_sub, sub


@given(value=st.integers())
def test_pipeline(value: int):
    expected = value + 1 - 1

    result = Pipeline(value).then(add(1)).then(sub(1)).unpack()

    assert (
        result == expected
    ), f"Pipeline does not produce expected result: {expected=} {result=}"


@pytest.mark.asyncio()
@given(value=st.integers())
async def test_async_pipeline(value: int):
    expected = value + 1 - 1

    result = (
        Pipeline(value)
        .then_async(async_add(1))
        .then_async(async_sub(1))
        .then(add(1))
        .then(sub(1))
        .unpack()
    )

    assert isawaitable(result), "AsyncPipeline returned non-awaitable object"
    assert (
        (await result) == expected
    ), f"AsyncPipeline does not produce expected result: {expected=} {result=}"


@pytest.mark.asyncio()
async def test_async_pipeline_with_non_possibly_non_async_result():
    await (
        Pipeline(Exception())
        .then_async(if_ok(async_add(1)))
        .then_async(if_ok(async_add(1)))
        .unpack()
    )
