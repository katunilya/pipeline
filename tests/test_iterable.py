import time
from typing import Iterable

import hypothesis.strategies as st
import pytest
from hypothesis import assume, given

import pipeline.iterable as i
from pipeline.pipeline import Pipeline


@given(
    data=st.iterables(st.integers(min_value=-100, max_value=100)),
    x=st.integers(),
)
def test_group_by(data: Iterable[int], x: int):
    assume(x != 0)

    result = Pipeline(data).then(i.group_by(lambda elem: str(elem % x))).unpack()

    for key, values in result.items():
        target = int(key)
        assert all([v % x == target for v in values])


@pytest.mark.asyncio
@given(
    data=st.iterables(st.integers(min_value=-100, max_value=100)),
    x=st.integers(),
)
async def test_group_by_async(data: Iterable[int], x: int):
    assume(x != 0)

    async def grouper(value: int):
        return str(value % x)

    result = await Pipeline(data).then_async(i.group_by_async(grouper)).unpack()

    for key, values in result.items():
        target = int(key)
        assert all([v % x == target for v in values])


@given(
    data=st.iterables(st.integers(min_value=-100, max_value=100)),
    x=st.integers(),
)
def test_filter(data: Iterable[int], x: int):
    result = Pipeline(data).then(i.filter(lambda elem: elem > x)).then(list).unpack()
    assert all([elem > x for elem in result])


@pytest.mark.asyncio
@given(
    data=st.iterables(st.integers(min_value=-100, max_value=100)),
    x=st.integers(),
)
async def test_filter_async(data: Iterable[int], x: int):
    async def predicate(item: int):
        return item > x

    result = await (
        Pipeline(data).then_async(i.filter_async(predicate)).then(list).unpack()
    )

    assert all([elem > x for elem in result])


@given(
    data=st.iterables(st.integers(min_value=-100, max_value=100)),
    x=st.integers(),
)
def test_valmap(data: Iterable[int], x: int):
    def mapper(item: int) -> int:
        return item + x

    result = Pipeline(data).then(i.valmap(mapper)).then(list).unpack()

    assert all([r == t + x for r, t in zip(result, data)])


@pytest.mark.asyncio
@given(
    data=st.lists(
        st.integers(min_value=-100, max_value=100),
        min_size=2,
        max_size=10,
        unique=True,
    ),
    x=st.integers(),
)
async def test_valmap_async(data: Iterable[int], x: int):
    async def mapper(item: int) -> int:
        return item + x

    result = await Pipeline(data).then_async(i.valmap_async(mapper)).then(set).unpack()
    target = set([t + x for t in data])

    assert result == target


@given(
    data=st.lists(
        st.integers(min_value=-100, max_value=100),
        min_size=2,
        max_size=5,
        unique=True,
    ),
    x=st.integers(),
)
def test_valmap_threaded(data: Iterable[int], x: int):
    def mapper(item: int) -> int:
        time.sleep(0.01)
        return item + x

    result = Pipeline(data).then(i.valmap_threaded(mapper)).then(set).unpack()
    target = set([t + x for t in data])

    assert result == target


@given(
    data=st.lists(
        st.integers(min_value=-100, max_value=100),
        min_size=2,
        max_size=5,
        unique=True,
    ),
    x=st.integers(min_value=1),
)
def test_skip(data: Iterable[int], x: int):
    assume(x < len(data))

    result = Pipeline(data).then(i.skip(x)).then(list).then(len).unpack()
    assert len(data) - x == result


@given(
    data=st.lists(
        st.integers(min_value=-100, max_value=100),
        min_size=2,
        max_size=5,
        unique=True,
    )
)
def test_reduce(data: Iterable[int]):
    assert (
        sum(data)
        == Pipeline(data).then(i.reduce(lambda x, y: x + y, initial=0)).unpack()
    )
